# README UFIX

## Start the project
Before starting you need to :
* Kill apache2 because they take our port 80 :
    * sudo systemctl stop apache2
* Install docker :
    * sudo apt-get update
    * sudo apt-get remove docker docker-engine docker.io
    * sudo apt install docker.io
* Install docker-compose :
    * sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    * sudo chmod +x /usr/local/bin/docker-compose

Dev / local :
* cp .env.template .env
* sudo docker-compose up -d
* sudo docker-compose exec app bash
* composer install
* yarn install
* yarn dev
* php bin/console make:migration
* php bin/console doctrine:migration:migrate
* php bin/console doctrine:fixtures:load
* add to /etc/hosts :
    * 127.0.0.1   app.localhost
    * 127.0.0.1   phpmyadmin.app.localhost
* http://app.localhost/
* http://phpmyadmin.app.localhost/

Watch auto :
* yarn run encore dev --watch

Prod :
* yarn run encore production --progress

## Naming of commits 

### Adding
* Adding a file : "add [file_name]"
* Adding code : 
    * Function : "add [function_name] [precision]"
    * Documentation : "add documentation [precision]" 

### Removing
* Removing a file : "delete [file_name]"
* Remove code : 
    * Function : "remove [function_name] [precision]"
    * Documentation : "remove documentation [precision]" 

### Editing
* Editing a file : "fixed [file_name]"
* Editing code : 
    * Function : "editing [function_name] [precision]"
    * Documentation : "editing documentation [precision]" 

### Merge/Conflicts
* Merge conflict : "fix conflict"

### Test code
* composer csfix && composer cscheck && composer phpstan